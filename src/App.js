import React, {Component} from "react";
import logo from "./logo.svg";
import "./App.css";
import {Route, Switch} from "react-router-dom";
import Layout from "./components/layout";
import Home from "./components/home";
import Product from "./components/product";

class App extends Component {
  render() {
    return (
      <Layout>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/index" component={Home} />
          <Route path="/home" component={Home} />
          <Route path="/product" component={Product} />
        </Switch>
      </Layout>
    );
  }
}

export default App;
