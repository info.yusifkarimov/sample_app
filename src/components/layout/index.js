import React, {Component} from "react";
import {BrowserRouter as Router} from "react-router-dom";
import Navbar from "../navbar";

class Layout extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Router>
        <Navbar />
        {this.props.children}
      </Router>
    );
  }
}

export default Layout;
