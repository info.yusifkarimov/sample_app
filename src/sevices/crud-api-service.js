import Axios from "axios";
import * as hostUrlService from "./host-url-service";

const options = {
  headers: {
    "Content-Type": "application/json",
  },
  token: "test_token",
};

let controllerName = "crud";
let partUrl = hostUrlService.sampleNoMainApiUrl + "/" + controllerName;

export const getAll = () => {
  return Axios.get(partUrl + "/getAll");
};

export const getById = (id) => {
  return Axios.get(partUrl + "/getById{id}");
};

export const save = (item) => {
  return Axios.post(partUrl + "/save", item, options);
};
